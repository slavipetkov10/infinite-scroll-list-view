import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

///https://www.youtube.com/watch?v=umIztKrk-AY&ab_channel=RobertBrunhage
void main() {
  runApp(MyApp());
}

// Flutter infinite scrolling ListView | HTTP GET
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> dogImages = List();
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    fetchFive();
    // With addListener we can access many of the options of ScrollController
    _scrollController.addListener(() {
      print(_scrollController.position.pixels);
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){
        // If we are at the bottom of the page
        fetchFive();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        controller: _scrollController,
        itemCount: dogImages.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            constraints: BoxConstraints.tightFor(height: 150.0),
            child: Image.network(
              dogImages[index],
              fit: BoxFit.fitWidth,
            ),
          );
        },
      ),
    );
  }
  fetch() async {
    final response = await http.get('https://dog.ceo/api/breeds/image/random');
    if(response.statusCode == 200){
      setState(() {
        // adds 5 images
        dogImages.add(json.decode(response.body)['message']);
      });
    }else{
      throw Exception('Failed to load images');
    }
  }
  fetchFive(){
    for(int i = 0; i < 5; i++){
      fetch();
    }
  }
}





























